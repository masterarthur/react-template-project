import { combineReducers } from 'redux'

import conterReducer from './counterReducer'

const rootReducer = combineReducers({
  counter: conterReducer,
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
