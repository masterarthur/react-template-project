import * as React from 'react'
import {
  FC,
  useState,
  useCallback,
} from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import ButtonGroup from '@material-ui/core/ButtonGroup'

import useStyle from './styles'

import { RootState } from './redux/reducers'
import { decrementAction, incrementAction, incrementAsyncAction } from './redux/actions/couterActions'
import { createAsyncAction } from './redux/helpers'
const App: FC = () => {
  const classes = useStyle()

  const [isLoading, setIsLoading] = useState(false)

  const currentValue = useSelector((state: RootState) => state.counter.curretValue)
  const dispatch = useDispatch()

  const onIncrementClick = useCallback(() => {
    dispatch(incrementAction({
      value: 3,
    }))
  }, [dispatch])

  const onDecrementClick = useCallback(() => {
    dispatch(decrementAction({
      value: 2,
    }))
  }, [dispatch])

  const onAsyncIncrementClick = useCallback(async () => {
    setIsLoading(true)
    try {
      await createAsyncAction(dispatch, incrementAsyncAction({
        value: 1,
      }))
    } finally {
      setIsLoading(false)
    }
  }, [dispatch])

  return (
    <Box className={classes.root}>
      <Typography>
        {currentValue}
      </Typography>
      <Box>
        <ButtonGroup disabled={isLoading}>
          <Button variant="contained" onClick={onIncrementClick}>
            Increment by 3
          </Button>
          <Button variant="contained" onClick={onDecrementClick}>
            Decrement by 2
          </Button>
          <Button variant="contained" onClick={onAsyncIncrementClick}>
            Increment Async
          </Button>
        </ButtonGroup>
      </Box>
    </Box>
  )
}

export default App
