import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { BrowserRouter, Route } from 'react-router-dom'
import { Provider as ReduxStoreProvider } from 'react-redux'

import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import { ThemeProvider } from '@material-ui/core/styles'

import store from './redux/store'

import App from './App'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#0d47a1',
      light: '#5472d3',
      dark: '#002171',
    },
    secondary: {
      main: '#bf360c',
      light: '#f9683a',
      dark: '#870000',
    },
  },
})

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <ReduxStoreProvider store={store}>
      <BrowserRouter>
        <Route component={App} exact path="/" />
      </BrowserRouter>
    </ReduxStoreProvider>
  </ThemeProvider>,
  document.querySelector('.root'),
)
